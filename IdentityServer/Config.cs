﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using System.Collections.Generic;
using IdentityServer4.Models;
using Microsoft.Extensions.Configuration;

namespace IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
            };
        }

        public static IEnumerable<ApiResource> GetApis()
        {
            return new List<ApiResource>
            {
                new ApiResource("jtapi", "JT API")
                {
                    Scopes = {new Scope("api.read")}
                }
            };
        }

        public static IEnumerable<Client> GetClients(IConfiguration configuration)
        {
            return new[]
            {
                new Client
                {
                    RequireConsent = false,
                    ClientId = configuration.GetSection("IdentitySettings:ClientId").Value,
                    ClientName = "Angular JT",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowedScopes = {"openid", "profile", "email", "api.read"},
                    RedirectUris = { configuration.GetSection("IdentitySettings:ClientRedirectUris").Value },
                    PostLogoutRedirectUris = {configuration.GetSection("IdentitySettings:LogoutRedirectUris").Value },
                    AllowedCorsOrigins = { configuration.GetSection("IdentitySettings:Cors").Value },
                    AllowAccessTokensViaBrowser = true,
                    AccessTokenLifetime = 3600,
                }
            };
        }
    }
}