﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using System;
using System.Linq;
using System.Security.Claims;
using IdentityModel;
using JT.DataAccess.Context;
using JT.Objects.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace IdentityServer
{
    public class SeedData
    {
        public static void EnsureSeedData(string connectionString)
        {
            var services = new ServiceCollection();
            services.AddEntityFrameworkNpgsql()
                .AddDbContext<JtDbContext>(options => options.UseNpgsql(connectionString));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<JtDbContext>()
                .AddDefaultTokenProviders();

            using (var serviceProvider = services.BuildServiceProvider())
            {
                using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var vollen = new UserInfo
                    {
                        Loggin = "vollen",
                        Password = "Qwer1234!",
                        GivenName = "Illon",
                        FamilyName = "Vollen",
                        Email = "e_vollen@bk.ru",
                        WebSite = "vollen.com",
                        AdressJson =
                            @"{ 'street_address': 'Sery', 'locality': 'Astana', 'postal_code': 69118, 'country': 'Kazakhstan' }"
                    };
                    AddFirstUser(scope, vollen);

                    var admin = new UserInfo
                    {
                        Loggin = "admin",
                        Password = "Qwer1234!",
                        GivenName = "admin",
                        FamilyName = "admin",
                        Email = "admin@bk.ru",
                        WebSite = "admin.com",
                        AdressJson =
                            @"{ 'street_address': 'admin', 'locality': 'Astana', 'postal_code': 69118, 'country': 'Kazakhstan' }"
                    };
                    AddFirstUser(scope, admin);
                }
            }
        }

        private static void AddFirstUser(IServiceScope scope, UserInfo userInfo)
        {
            var userMgr = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var user = userMgr.FindByNameAsync(userInfo.Loggin).Result;
            if (user == null)
            {
                user = new ApplicationUser
                {
                    UserName = userInfo.Loggin,
                    Email = userInfo.Email,
                    EmailConfirmed = true
                };
                var result = userMgr.CreateAsync(user, userInfo.Password).Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                result = userMgr.AddClaimsAsync(user, new Claim[]
                {
                    new Claim(JwtClaimTypes.Name, $"{userInfo.GivenName} {userInfo.FamilyName}"),
                    new Claim(JwtClaimTypes.GivenName, userInfo.GivenName),
                    new Claim(JwtClaimTypes.FamilyName, userInfo.FamilyName),
                    new Claim(JwtClaimTypes.Email, userInfo.Email),
                    new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                    new Claim(JwtClaimTypes.WebSite, userInfo.WebSite),
                    new Claim(JwtClaimTypes.Address, userInfo.AdressJson,
                        IdentityServer4.IdentityServerConstants.ClaimValueTypes.Json)
                }).Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                Console.WriteLine($"{userInfo.Loggin} создан");
            }
            else
            {
                Console.WriteLine($"{userInfo.Loggin} уже существует");
            }
        }

        private struct UserInfo
        {
            public string Loggin { get; set; }
            public string Password { get; set; }
            public string GivenName { get; set; }
            public string FamilyName { get; set; }
            public string Email { get; set; }
            public string WebSite { get; set; }
            public string AdressJson { get; set; }
        }
    }
}
