import { Category } from './category';
import { Invoice } from './invoice';

export class Product {
    id: string;
    name: string;
    article: number;
    cost: number;
    quantity: number;
    location: string;
    updateDate: Date;
    category: Category;
    invoices: Invoice[];
// Для счет-фактур
    invoice: Invoice;
    productId: string;
    changeQuantity: number;
  }
