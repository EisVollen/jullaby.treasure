import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';

import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Product } from '../objects/product';
import { AuthService } from './auth.service';
import { DialogService } from '../dialog/dialog.service';


@Injectable({
  providedIn: 'root',
})

export class ProductService {
  public products: Product[];
  token: string;
  httpOptionsToken: {};

  constructor(private httpClient: HttpClient ,
              private authService: AuthService,
              private dialog: DialogService) {
       this.token = this.authService.authorizationHeaderValue;
       this.httpOptionsToken  = {
           headers: new HttpHeaders({
            'Content-Type':  'application/json',
            // tslint:disable-next-line:object-literal-key-quotes
            'Authorization': this.token,
            responseType: 'json'
     }, )
   };
  }

  getProductByCategories(id: string): Observable<any> {
    return this.httpClient.get('api/Product/category/' + id,  this.httpOptionsToken)
    .pipe(
      map(res => res),
      catchError(err => this.dialog.handleError(err))
    );
   }

    updateProduct(product: Product): Observable<any> {

      return this.httpClient.put('api/Product/Update', product, this.httpOptionsToken).pipe(
          map(res => res),
          catchError(err => this.dialog.handleError(err))
        );
      }

    changeQuantityProduct(product: Product): Observable<any> {

      return this.httpClient.put('api/Product/ChangeQuantity', product, this.httpOptionsToken).pipe(
          map(res => res),
          catchError(err => this.dialog.handleError(err))
        );
      }

    saveProduct(product: Product): Observable<any> {
        return this.httpClient.post('api/Product', product, this.httpOptionsToken).pipe(
          map(res => res),
          catchError(err => this.dialog.handleError(err))
        );
      }

  deleteProduct(product: Product): Observable<any> {
    const params = new HttpParams()
    .set('id', product.id);
    const option = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
         // tslint:disable-next-line:object-literal-key-quotes
        'Authorization': this.token,
        responseType: 'json'
      }, ),
      params
    };

    return this.httpClient.request('delete', 'api/Product', option).pipe(
          map(res => res),
          catchError(err => this.dialog.handleError(err))
        );
      }

  getProductById(id: string): Observable<any> {

    return this.httpClient.get('api/Product/' + id, this.httpOptionsToken)
      .pipe(
        map(res => res),
        catchError(err => this.dialog.handleError(err))
       );
    }

    searchProducts(text: string): Observable<any> {
      const params = new HttpParams()
      .set('text', text);
      const option = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
           // tslint:disable-next-line:object-literal-key-quotes
          'Authorization': this.token,
          responseType: 'json'
        }, ),
        params
      };
      return this.httpClient.get('api/Product/' + text, option)
        .pipe(
          map(res => res),
          catchError(err => this.dialog.handleError(err))
         );
      }
}
