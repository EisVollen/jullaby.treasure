import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { UserManager, UserManagerSettings, User, WebStorageStateStore } from 'oidc-client';
import { CookieStorage } from 'cookie-storage';
import { BehaviorSubject } from 'rxjs';
import {Router} from '@angular/router';
import { DialogService } from '../dialog/dialog.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // Observable navItem source
  private authNavStatusSource = new BehaviorSubject<boolean>(false);
  // Observable navItem stream
  authNavStatus$ = this.authNavStatusSource.asObservable();

  private manager = new UserManager(getClientSettings());
  private user: User | null;

  constructor(private http: HttpClient,
              private router: Router,
              private dialog: DialogService) {

    this.router = router;
    this.manager.getUser().then(user => {
      this.user = user;
      this.authNavStatusSource.next(this.isAuthenticated());
    });
  }

  login() {
    return this.manager.signinRedirect();
  }

  async completeAuthentication() {
      this.user = await this.manager.signinRedirectCallback();
      this.authNavStatusSource.next(this.isAuthenticated());
  }

  register(userRegistration: any) {
    return this.http.post('http://localhost:5000/account', userRegistration)
    .pipe(
      map(() => this.completeAuthentication()),
    catchError(err => this.dialog.handleError(err)));
  }

  isAuthenticated(): boolean {
    return this.user != null && !this.user.expired;
  }

  get authorizationHeaderValue(): string {
    if (this.user == null) {
      this.router.navigateByUrl('/login');
    }
    return `${this.user.token_type} ${this.user.access_token}`;
  }

  get name(): string {
    return this.user != null ? this.user.profile.name : '';
  }

  signout() {
    this.manager.signoutRedirect();
  }
}


export function getClientSettings(): UserManagerSettings {
  return {
    authority: 'http://localhost:5000',
    client_id: 'spa',
    redirect_uri: 'http://localhost:4200/callback',
    post_logout_redirect_uri: 'http://localhost:4200/',
    response_type: 'id_token token',
    scope: 'openid profile email api.read',
    filterProtocolClaims: true,
    loadUserInfo: true,
    automaticSilentRenew: true,
    silent_redirect_uri: 'http://localhost:4200/callback',
    userStore: new WebStorageStateStore({ store: new CookieStorage() })
  };
}
