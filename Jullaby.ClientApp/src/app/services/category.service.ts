import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';

import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Category } from '../objects/category';
import { AuthService } from './auth.service';
import { DialogService } from '../dialog/dialog.service';

@Injectable({
  providedIn: 'root',
})

export class CategoryService {
  public categories: Category[];
  token: string;
  httpOptionsToken: {};

  constructor(private httpClient: HttpClient ,
              private authService: AuthService,
              private dialog: DialogService) {

             this.token = this.authService.authorizationHeaderValue;
             this.httpOptionsToken  = {
              headers: new HttpHeaders({
                'Content-Type':  'application/json',
                 // tslint:disable-next-line:object-literal-key-quotes
                'Authorization': this.token,
                responseType: 'json'
              }, )
            };
          }

  getCategories(): Observable<any> {
    return this.httpClient.get('api/Category', this.httpOptionsToken)
      .pipe(
        map(res => res),
        catchError(err => this.dialog.handleError(err))
       );
    }

    updateCategory(category: Category): Observable<any> {

      return this.httpClient.put('api/Category', category, this.httpOptionsToken).pipe(
          map(res => res),
          catchError(err => this.dialog.handleError(err))
        );
      }

    saveCategory(category: Category): Observable<any> {
        return this.httpClient.post('api/Category', category, this.httpOptionsToken).pipe(
          map(res => res),
          catchError(err => this.dialog.handleError(err))
        );
      }

      deleteCategory(category: Category): Observable<any> {
        const params = new HttpParams()
        .set('id', category.id);
        const option = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json',
             // tslint:disable-next-line:object-literal-key-quotes
            'Authorization': this.token,
            responseType: 'json'
          }, ),
          params
        };

        return this.httpClient.request('delete', 'api/Category', option)
        .pipe(
          map(res => res),
          catchError(err => this.dialog.handleError(err))
        );
      }

  getCategoryById(id: string): Observable<any> {
    return this.httpClient.get('api/Category/' + id,  this.httpOptionsToken)
      .pipe(
        map(res => res),
        catchError(err => this.dialog.handleError(err))
       );
    }
  }
