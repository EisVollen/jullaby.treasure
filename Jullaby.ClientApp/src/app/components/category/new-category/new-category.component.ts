import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../../services/category.service';
import { Category } from '../../../objects/category';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DialogService } from 'src/app/dialog/dialog.service';

@Component({
  selector: 'app-new-category',
  templateUrl: './new-category.component.html',
  styleUrls: ['./new-category.component.css']
})
export class NewCategoryComponent implements OnInit {

  constructor(
    private categoryService: CategoryService,
    private route: ActivatedRoute,
    private location: Location,
    private dialog: DialogService
  ) {}

  ngOnInit() {}

  save(name: string): void {
    console.log(name);
    console.log('saveCategory');
    if (!name) {
      return;
    }
    this.categoryService
      .saveCategory({ name } as Category)
      .subscribe(() => {
         this.dialog.success('Категория успешно добавлена!');
         this.goBack();
        });
  }

  goBack(): void {
    this.location.back();
  }
}
