import { Component, OnInit, Input } from '@angular/core';
import { Category } from '../../../objects/category';

import { CategoryService } from '../../../services/category.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DialogService } from 'src/app/dialog/dialog.service';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.css']
})
export class CategoryDetailComponent implements OnInit {
  @Input() category: Category;
            id: string;

constructor(private categoryService: CategoryService,
            private route: ActivatedRoute,
            private location: Location,
            private dialog: DialogService) {
               this.id = this.route.snapshot.paramMap.get('id');
               this.getCategoryById();
              }

  ngOnInit() {
  }

  getCategoryById() {
    this.categoryService.getCategoryById(this.id)
    .subscribe(
      result => { this.category = result; console.log(this.category); },
      error => console.error(error));
    }

    goBack(): void {
      this.location.back();
    }

   save(): void {
    console.log(this.category);
    console.log('save');
    this.categoryService.updateCategory(this.category)
          .subscribe(() => {
            this.dialog.success('Категория изменена!');
            this.goBack();
           });
        }

    delete(): void {
     this.categoryService.deleteCategory(this.category)
          .subscribe(() =>  {
            this.dialog.success('Категория успешно удалена!');
            this.goBack();
           });
  }
}
