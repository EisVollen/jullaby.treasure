import { Component, OnInit } from '@angular/core';
import { Category } from '../../../objects/category';
import { CategoryService } from '../../../services/category.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  categories: Category[];
  selectedCategory: Category;

  constructor(private categoryService: CategoryService) { }

  ngOnInit() {
    this.getCategories().subscribe(
      result => { this.categories = result; },
    error => console.error(error));
  }

  getCategories(): Observable<any> {
    return this.categoryService.getCategories();
  }

  onSelect(category: Category): void {
    this.selectedCategory = category;
  }
}
