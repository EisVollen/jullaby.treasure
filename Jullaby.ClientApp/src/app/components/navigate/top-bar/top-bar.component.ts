import { Component, OnInit, SimpleChanges } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {

  title = 'Jullaby.Treasure';
  name: string;
  isAuthenticated: boolean;
  subscription: Subscription;

  constructor(private authService: AuthService) {}
 ngOnInit() {
    this.name = this.authService.name;
  }

}
