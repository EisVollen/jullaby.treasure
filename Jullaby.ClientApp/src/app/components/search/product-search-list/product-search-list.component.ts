import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/objects/product';
import { ProductService } from 'src/app/services/product.service';
import { ActivatedRoute } from '@angular/router';
import { DialogService } from 'src/app/dialog/dialog.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-product-search-list',
  templateUrl: './product-search-list.component.html',
  styleUrls: ['./product-search-list.component.css']
})
export class ProductSearchListComponent implements OnInit {
  name: string;
  products: Product[];
  notFound: boolean;

  constructor(private productService: ProductService,
              private route: ActivatedRoute,
              private location: Location,
              private dialog: DialogService) {
     this.name = this.route.snapshot.paramMap.get('name');
     this.searchProducts();
    }

  ngOnInit() {
  }

  searchProducts() {
    this.productService.searchProducts(this.name)
    .subscribe(
      result => { this.products = result;
                  if (this.products.length === 0) {
                    this.notFound = true;
                   } },
      error => this.dialog.handleError(error));
    }
 }
