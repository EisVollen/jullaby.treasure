import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.css']
})
export class ProductSearchComponent implements OnInit {

  isAuthenticated: boolean;
  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
  }

  onSearch(text: string) {
    this.router.navigateByUrl('/search-product/' + text);
  }
}
