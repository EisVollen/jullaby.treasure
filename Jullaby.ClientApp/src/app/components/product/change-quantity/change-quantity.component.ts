import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../../objects/product';
import { ProductService } from '../../../services/product.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Invoice } from '../../../objects/invoice';
import { DialogService } from 'src/app/dialog/dialog.service';

@Component({
  selector: 'app-change-quantity',
  templateUrl: './change-quantity.component.html',
  styleUrls: ['./change-quantity.component.css']
})
export class ChangeQuantityComponent implements OnInit {
  @Input() product: Product;
  id: string;
  invoice: Invoice;

  constructor(private productService: ProductService,
              private route: ActivatedRoute,
              private location: Location,
              private dialog: DialogService) {
       this.id = this.route.snapshot.paramMap.get('id');
       this.getProductById();
       this.invoice = new Invoice();
      }


  ngOnInit() {
  }

  getProductById() {
    this.productService.getProductById(this.id)
    .subscribe(
      result => { this.product = result; },
      error => console.error(error));
    }

    goBack(): void {
      this.location.back();
    }

   save(quantity: number): void {
    this.product.invoice = this.invoice;
    this.product.productId = this.product.id;
    this.product.changeQuantity = quantity;
    this.productService.changeQuantityProduct(this.product)
          .subscribe(() => {
            this.dialog.success('Количество товара изменено!\nСчет фактура добавлена.');
            this.goBack();
           });
        }
}
