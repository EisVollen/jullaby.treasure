import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../../objects/product';
import { ProductService } from '../../../services/product.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DialogService } from 'src/app/dialog/dialog.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  @Input() product: Product;
  id: string;
  constructor(private productService: ProductService,
              private route: ActivatedRoute,
              private location: Location,
              private dialog: DialogService) {
       this.id = this.route.snapshot.paramMap.get('id');
       this.getProductById();
      }

  ngOnInit() {
  }

  getProductById() {
    this.productService.getProductById(this.id)
    .subscribe(
      result => { this.product = result; },
      error => console.error(error));
    }

    goBack(): void {
      this.location.back();
    }

   save(): void {
      this.productService.updateProduct(this.product)
          .subscribe(() => {
          this.dialog.success('Товар успешно сохранен!');
          this.goBack();
         });
        }

    delete(): void {
     this.productService.deleteProduct(this.product)
          .subscribe(() => {
            this.dialog.success('Товар успешно удален!');
            this.goBack();
           });
  }
}
