import { Component, OnInit } from '@angular/core';
import { Category } from '../../../objects/category';
import { Product } from '../../../objects/product';

import { ProductService } from '../../../services/product.service';
import { CategoryService } from '../../../services/category.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DialogService } from 'src/app/dialog/dialog.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  id: string;

  category: Category;
  products: Product[];
  selectedProduct: Product;

  constructor(private productService: ProductService,
              private categoryService: CategoryService,
              private route: ActivatedRoute,
              private location: Location,
              private dialog: DialogService) {
               this.id = this.route.snapshot.paramMap.get('id');
               this.getCategoryById();
              }

ngOnInit() {
    this.getProductByCategories();
  }

  getProductByCategories() {
    this.productService.getProductByCategories(this.id)
    .subscribe(
      result => { this.products = result; },
      error => this.dialog.handleError(error));
    }

    getCategoryById() {
    this.categoryService.getCategoryById(this.id)
    .subscribe(
      result => { this.category = result; },
      error => this.dialog.handleError(error));
    }

  onSelect(product: Product): void {
    this.selectedProduct = product;
  }

}
