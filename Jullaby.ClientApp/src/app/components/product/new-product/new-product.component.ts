import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../../objects/product';
import { ProductService } from '../../../services/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Category } from '../../../objects/category';
import { CategoryService } from '../../../services/category.service';
import { DialogService } from 'src/app/dialog/dialog.service';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.css']
})
export class NewProductComponent implements OnInit {

  @Input() category: Category;
    id: string;
  constructor(private categoryService: CategoryService,
              private productService: ProductService,
              private route: ActivatedRoute,
              public router: Router,
              private dialog: DialogService) {
       this.id = this.route.snapshot.paramMap.get('id');
       this.getCategoryById();
      }

  ngOnInit() {
  }

  getCategoryById() {
    this.categoryService.getCategoryById(this.id)
    .subscribe(
      result => { this.category = result; },
      error => console.error(error));
    }

    goBack(): void {
      this.router.navigate(['/products/' + this.category.id]);
    }

   save(name: string, article: number, cost: number, location: string): void {
        if (!name || !article || !cost || !location) {
            return;
          }
        this.productService.saveProduct({ name, article, cost, location, category: this.category } as Product)
            .subscribe(() => {
              this.dialog.success('Товар успешно добавлен!');
              this.goBack();
             });
        }

}
