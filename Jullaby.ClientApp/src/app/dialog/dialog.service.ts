import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  handleError(error): any {
    let errorMessage = '';
    if (error.error !== '') {
      errorMessage = `Ошибка: ${error.error}`;
    } else {
      errorMessage = `Код ошибки: ${error.status}\nОшибка: ${error.message}`;
    }
    Swal.fire({
      type: 'error',
      title: 'Ой...',
      text: errorMessage,
    });
    return throwError(error);
  }

  success(message: string) {
    Swal.fire({
      position: 'top-end',
      type: 'success',
      title: message,
      showConfirmButton: false,
      timer: 1500
    });
  }
 }
