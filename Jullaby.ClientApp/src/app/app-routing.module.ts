import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoriesComponent } from './components/category/categories/categories.component';
import { CategoryDetailComponent } from './components/category/category-detail/category-detail.component';
import { ProductListComponent } from './components/product/product-list/product-list.component';
import { ProductDetailComponent } from './components/product/product-detail/product-detail.component';
import { NewCategoryComponent } from './components/category/new-category/new-category.component';
import { ChangeQuantityComponent } from './components/product/change-quantity/change-quantity.component';
import { NewProductComponent } from './components/product/new-product/new-product.component';
import { AuthCallbackComponent } from './components/account/auth-callback/auth-callback.component';
import { LoginComponent } from './components/account/login/login.component';
import { ProductSearchListComponent } from './components/search/product-search-list/product-search-list.component';

const routes: Routes = [
{ path: '', redirectTo: '/dashboard', pathMatch: 'full' },
{ path: 'dashboard', component: CategoriesComponent },
{ path: 'products/:id', component: ProductListComponent },
{ path: 'edit-product/:id', component: ProductDetailComponent },
{ path: 'change-quantity/:id', component: ChangeQuantityComponent },
{ path: 'new-product/:id', component: NewProductComponent },
{ path: 'search-product/:name', component: ProductSearchListComponent },
{ path: 'new-category', component: NewCategoryComponent },
{ path: 'edit-category/:id', component: CategoryDetailComponent },
{ path: 'callback', component: AuthCallbackComponent },
{ path: 'login', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
