import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {HttpModule} from '@angular/http';
import { SweetAlert2Module } from '../../swal/public_api';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CategoriesComponent } from './components/category/categories/categories.component';
import { CategoryDetailComponent } from './components/category/category-detail/category-detail.component';
import { ProductListComponent } from './components/product/product-list/product-list.component';
import { ProductDetailComponent } from './components/product/product-detail/product-detail.component';
import { TopBarComponent } from './components/navigate/top-bar/top-bar.component';
import { NewCategoryComponent } from './components/category/new-category/new-category.component';
import { ChangeQuantityComponent } from './components/product/change-quantity/change-quantity.component';
import { NewProductComponent } from './components/product/new-product/new-product.component';
import { AgGridModule } from 'ag-grid-angular';
import { AuthCallbackComponent } from './components/account/auth-callback/auth-callback.component';
import { LoginComponent } from './components/account/login/login.component';
import { ProductSearchComponent } from './components/search/product-search/product-search.component';
import { ProductSearchListComponent } from './components/search/product-search-list/product-search-list.component';
import { BackComponent } from './components/navigate/back/back.component';

@NgModule({
  declarations: [
    AppComponent,
    CategoriesComponent,
    CategoryDetailComponent,
    ProductListComponent,
    ProductDetailComponent,
    TopBarComponent,
    NewCategoryComponent,
    ChangeQuantityComponent,
    NewProductComponent,
    AuthCallbackComponent,
    LoginComponent,
    ProductSearchComponent,
    ProductSearchListComponent,
    BackComponent
  ],
  imports: [
    BrowserModule,
    AgGridModule.withComponents([]),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    HttpModule,
    SweetAlert2Module.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
