﻿using JT.Objects.Core;
using JT.Objects.User;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace JT.DataAccess.Context
{
    public class JtDbContext : IdentityDbContext<ApplicationUser>
    {
        public JtDbContext(DbContextOptions<JtDbContext> options)
            : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<ProductInvoice> ProductsInvoices { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ProductInvoice>()
                .HasOne(pt => pt.Product)
                .WithMany("ProductsInvoices")
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ProductInvoice>()
                .HasOne(pt => pt.Invoice)
                .WithMany("ProductsInvoices");

        }
        
    }
}
