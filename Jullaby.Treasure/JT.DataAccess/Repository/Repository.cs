﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using JT.DataAccess.Context;
using JT.Objects.Core;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace JT.DataAccess.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private readonly JtDbContext _context;
        
        public Repository(JtDbContext context)
        {
            _context = context;
        }
        
        public async Task Update(TEntity entity)
        {
            try
            {
                _context.Set<TEntity>().Update(entity);
                _context.Entry(entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                Log.Information($"{entity.GetType()}, Id = {entity.Id} обновлена ");
            }
            catch (DbException ex)
            {
                Log.Error(ex, $"Ошибка при сохранении {entity.GetType()} Id={entity.Id}");
            }
        }

        public async Task<IEnumerable<TEntity>> ListAsync(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<string> includeProperties = null)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();
            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProperties != null)
                foreach (var includeProperty in includeProperties)
                {
                    query = query.Include(includeProperty);
                }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            return await query.ToListAsync();
        }

        public async Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> filter,
            List<string> includeProperties = null)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();
            if (includeProperties == null) return await query.FirstOrDefaultAsync(filter);

            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return await query.FirstOrDefaultAsync(filter);
        }

        public async Task<TEntity> GetByIdAsync(Guid id,
            List<string> includeProperties = null)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();
            if (includeProperties == null) return await query.FirstOrDefaultAsync(p => p.Id == id);
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return await query.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task DeleteAsync(Guid id)
        {
            await DeleteAsync(await GetByIdAsync(id));
        }

        public async Task DeleteAsync(TEntity entity)
        {
            try
            {
                _context.Set<TEntity>().Remove(entity);
                await _context.SaveChangesAsync();
                Log.Information($"{entity.GetType()} Id = {entity.Id} удалена");
            }
            catch (DbException ex)
            {
                Log.Error(ex, $"Ошибка при удалении {entity.GetType()} Id={entity.Id}");
            }
        }

        public async Task<bool> IsExistsAsync(Expression<Func<TEntity, bool>> filter)
        {
            return await _context.Set<TEntity>().AnyAsync(filter);
        }

        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}