﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JT.DataAccess.Repository
{
   public interface IRepository<TEntity>: IDisposable
   {
      
       Task<IEnumerable<TEntity>> ListAsync(Expression<Func<TEntity, bool>> filter = null,
           Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
           List<string> includeProperties = null);
       Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> filter,
           List<string> includeProperties = null);
       Task<TEntity> GetByIdAsync(Guid id,
           List<string> includeProperties = null);
       Task Update(TEntity entity);
       Task DeleteAsync(Guid id);
       Task DeleteAsync(TEntity entity);
       Task<bool> IsExistsAsync(Expression<Func<TEntity, bool>> filter);
   }
}
