﻿using System;
using System.Threading.Tasks;
using JT.Objects.Core;

namespace JT.DataAccess.Repository
{
    public interface IObjectCreator<in TEntity>: IDisposable
    {
        Task SaveAsync(TEntity entity);
    }
}
