﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using JT.DataAccess.Context;
using JT.Objects.Core;
using Serilog;

namespace JT.DataAccess.Repository
{
    /// <summary>
    /// Из-за ограничения ef при сохранении связанных сущностей
    /// вынесла в отдельный класс методы сохранения
    /// </summary>
    public class ObjectCreator<TEntity>: IObjectCreator<TEntity> where TEntity:Entity
    {
        private readonly JtDbContext _context;

        public ObjectCreator(JtDbContext context)
        {
            _context = context;
        }

        //Добавлять новый тип только если необходимо сохранять relation 
        //Ограничение ef
        //Для всего остального дефолтный метод подойдет
        public async Task SaveAsync(TEntity entity)
        {
            entity.Id = Guid.NewGuid();

            switch (entity)
            {
                case Product product:
                    await ProductSaveAsync(product);
                    break;
                case ProductInvoice productInvoice:
                    await ProductInvoiceSaveAsync(productInvoice);
                    break;
                default:
                    await EntitySaveAsync(entity);
                    break;
            }
        }

        private async Task ProductInvoiceSaveAsync(ProductInvoice productInvoice)
        {
            try
            {
                _context.Invoices.Attach(productInvoice.Invoice);
                _context.Products.Attach(productInvoice.Product);
                _context.ProductsInvoices.Add(productInvoice);
                await _context.SaveChangesAsync();

                Log.Information($"{productInvoice.GetType()}, Id = {productInvoice.Id} была добавлена ");
            }
            catch (DbException ex)
            {
                Log.Error(ex, $"Ошибка при добавлении {productInvoice.GetType()} Id={productInvoice.Invoice}");
            }
        }

        private async Task ProductSaveAsync(Product product)
        {
            try
            {
                _context.Categories.Attach(product.Category);
                _context.Products.Add(product);
                await _context.SaveChangesAsync();

                Log.Information($"{product.GetType()}, Id = {product.Id} была добавлена ");
            }
            catch (DbException ex)
            {
                Log.Error(ex, $"Ошибка при добавлении {product.GetType()} Id={product.Id}");
            }
        }

        private async Task EntitySaveAsync(TEntity entity)
        {

            try
            {
                _context.Set<TEntity>().Add(entity);
                await _context.SaveChangesAsync();
                Log.Information($"{entity.GetType()}, Id = {entity.Id} была добавлена ");
            }
            catch (DbException ex)
            {
                Log.Error(ex, $"Ошибка при добавлении {entity.GetType()} Id={entity.Id}");
            }
        }
        
        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}
