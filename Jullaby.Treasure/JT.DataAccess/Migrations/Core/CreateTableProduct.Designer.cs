﻿using System;
using JT.DataAccess.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace JT.DataAccess.Migrations.Core
{
    [DbContext(typeof(JtDbContext))]
    [Migration("CreateTableProduct")]
    partial class CreateTableProduct
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {

            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("JT.Objects.Core.Product", b =>
            {
                b.Property<Guid>("Id")
                    .ValueGeneratedOnAdd();

                b.Property<string>("Name");
                b.Property<uint>("Article");
                b.Property<uint>("Cost");
                b.Property<uint>("Quantity");
                b.Property<string>("Location");
                b.Property<Guid>("CategoryId");
                b.Property<DateTime>("UpdateDate");

                b.HasKey("Id");

                b.ToTable("Products");
            });
           
        }

    }
}
