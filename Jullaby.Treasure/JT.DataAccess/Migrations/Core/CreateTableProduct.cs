﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JT.DataAccess.Migrations.Core
{
    public partial class CreateTableProduct: Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "Products",
                table => new
                {
                    Id = table.Column<Guid>(),
                    Name = table.Column<string>(maxLength: 200),
                    Article = table.Column<uint>(),
                    Cost = table.Column<uint>(),
                    Quantity = table.Column<uint>(),
                    Location = table.Column<string>(maxLength: 1000),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CategoryId = table.Column<Guid>(),

                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        "Category_Id",
                        x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id");
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable("Products");
        }
    }
}
