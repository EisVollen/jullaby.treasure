﻿using System;
using JT.DataAccess.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace JT.DataAccess.Migrations.Core
{
    [DbContext(typeof(JtDbContext))]
    [Migration("CreateTableProductsInvoices")]
    partial class CreateTableProductsInvoices
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {

            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("JT.Objects.Core.ProductInvoice", b =>
            {
                b.Property<Guid>("Id")
                    .ValueGeneratedOnAdd();
                
                b.Property<Guid>("ProductId");
                b.Property<Guid>("InvoiceId");
                b.Property<int>("Quantity");

                b.HasKey("Id");

                b.ToTable("ProductsInvoices");
            });
           
        }

    }
}
