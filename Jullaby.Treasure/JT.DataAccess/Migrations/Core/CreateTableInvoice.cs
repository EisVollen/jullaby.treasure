﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JT.DataAccess.Migrations.Core
{
    public partial class CreateTableInvoice: Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "Invoices",
                table => new
                {
                    Id = table.Column<Guid>(),
                    Number = table.Column<string>(maxLength: 100),
                    CreateDate = table.Column<DateTime>()

                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoices", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable("Invoices");
        }
    }
}
