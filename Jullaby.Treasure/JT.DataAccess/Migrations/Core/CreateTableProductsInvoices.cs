﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JT.DataAccess.Migrations.Core
{
    public partial class CreateTableProductsInvoices : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "ProductsInvoices",
                table => new
                {
                    Id = table.Column<Guid>(),
                    ProductId = table.Column<Guid>(),
                    InvoiceId = table.Column<Guid>(),
                    Quantity = table.Column<int>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductInvoice", x => x.Id);
                    table.ForeignKey(
                        "Product_Id",
                        x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id");
                    table.ForeignKey(
                        "Invoice_Id",
                        x => x.InvoiceId,
                        principalTable: "Invoices",
                        principalColumn: "Id");
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable("ProductsInvoices");
        }
    }
}
