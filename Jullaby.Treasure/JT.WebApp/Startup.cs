using System;
using AutoMapper;
using JT.DataAccess.Context;
using JT.DataAccess.Repository;
using JT.Objects.Mapping;
using JT.Objects.User;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;

namespace JT.WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            
            ConfigureDb(services);
            ConfigureIdentityServer(services);
            ConfigurationAutoMapper(services);
            ConfigureSwagger(services);
            ConfigureInterfaces(services);
        }

        private void ConfigureInterfaces(IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IObjectCreator<>), typeof(ObjectCreator<>));
        }

        // Auto Mapper Configurations
        private void ConfigurationAutoMapper(IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MappingProfile()); });

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        //Create Client for IdentityServer
        private void ConfigureIdentityServer(IServiceCollection services)
        {
            services.AddDefaultIdentity<ApplicationUser>()
                .AddEntityFrameworkStores<JtDbContext>();
            services.AddMvc();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(o =>
            {
                o.Authority = "http://localhost:5000";
                o.Audience = "jtapi";
                o.RequireHttpsMetadata = false;
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("ApiReader", policy => policy.RequireClaim("scope", "api.read"));
            });
        }

        //Connect db
        private void ConfigureDb(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("DbConnectionString");
            services.AddEntityFrameworkNpgsql()
                .AddDbContext<JtDbContext>(options => options.UseNpgsql(connectionString));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {



            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseCors(options => options.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
            app.UseAuthentication();
            MigrateDatabase(app);
            ConfigureSwagger(app);
            
            MapRouteProduct(app);
            
        }

        private static void MapRouteProduct(IApplicationBuilder app)
        {
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "Default",
                    "{action}/{id}",
                    new
                    {
                        controller = "Swagger",
                        action = "Index"
                    }
                );
            });
        }

        private static void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info {Title = "Jullaby.Treasure.Swagger", Version = "v1"});
            });
        }

        private static void ConfigureSwagger(IApplicationBuilder app)
        {
            app.UseSwagger(c =>
            {
                c.PreSerializeFilters.Add((swagger, httpReq) => swagger.Host = httpReq.Host.Value);
            });
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "swagger";
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1 Docs");
            });
            app.UseSwagger();

        }

        private void MigrateDatabase(IApplicationBuilder app)
        {
            try
            {
                if (!bool.TryParse(Configuration.GetSection("AppSettings:UseMigration").Value, out var isMigrate) ||
                    !isMigrate) return;
                using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
                    .CreateScope())
                {
                    serviceScope.ServiceProvider.GetService<JtDbContext>()
                        .Database.Migrate();
                }
            }
            catch (Exception e)
            {
                Log.Error(e, "������ ��� �������� ��!");
            }
        }
    }
}
