﻿using Microsoft.AspNetCore.Mvc;

namespace JT.WebApp.Controllers
{
     public class SwaggerController : Controller
    {
        [Route("")]
        [HttpGet]
         public IActionResult Index()
        {
            return Redirect("/swagger/index.html");
        }
    }
}
