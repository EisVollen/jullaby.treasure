﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using JT.DataAccess.Repository;
using JT.Objects.Core;
using JT.Objects.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace JT.WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CategoryController : ControllerBase
    {
        private readonly IRepository<Category> _categoryRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IObjectCreator<Category> _objectCreator;
        private readonly IMapper _mapper;

        public CategoryController(
            IRepository<Category> categoryRepository,
            IMapper mapper, 
            IRepository<Product> productRepository, 
            IObjectCreator<Category> objectCreator)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
            _productRepository = productRepository;
            _objectCreator = objectCreator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            //sort by Name
            var categories = await _categoryRepository.ListAsync(orderBy: o => o.OrderBy(p => p.Name));
            //map data
            return new JsonResult(categories.Select(category => _mapper.Map<CategoryDto>(category)).ToList());
        }


        [HttpGet("{name}")]
        public async Task<IActionResult> GetByName(string name)
        {
            var categories = await _categoryRepository.ListAsync(x => x.Name.ToLower().Contains(name.ToLower()));

            var categoriesDto = categories.ToList().Select(category => _mapper.Map<CategoryDto>(category)).ToList();
            if (!categoriesDto.Any())
            {
                return NotFound();
            }

            return new JsonResult(categoriesDto);
        }

        [HttpGet("{id:regex([[0-9A-Fa-f]]{{8}}[[-]]?([[0-9A-Fa-f]]{{4}}[[-]]?){{3}}[[0-9A-Fa-f]]{{12}})}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var category = await _categoryRepository.GetByIdAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            return new JsonResult(_mapper.Map<CategoryDto>(category));
        }

        [HttpPost]
        public async Task<IActionResult> Create([Bind("Name")] CategoryDto categoryDto)
        {
            if (!ModelState.IsValid) return BadRequest();

            var isCategoryExist =
                await _categoryRepository.IsExistsAsync(p => p.Name.ToLower().Contains(categoryDto.Name.ToLower()));
            if (isCategoryExist) return Conflict();

            try
            {
                await _objectCreator.SaveAsync(_mapper.Map<Category>(categoryDto));
                return Ok();
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Ошибка при добавлении {categoryDto.Name}");
                return StatusCode(520);
            }

        }

        [HttpPut]
        public async Task<IActionResult> Update([Bind("Id,Name")] CategoryDto categoryDto)
        {
            if (!ModelState.IsValid) return BadRequest();

            var isCategoryExist = await _categoryRepository.IsExistsAsync(p => p.Id == categoryDto.Id);
            if (!isCategoryExist)
            {
                return NotFound();
            }

            try
            {
                await _categoryRepository.Update(_mapper.Map<Category>(categoryDto));
                return Ok();
            }
            catch (Exception ex)
            {
                Log.Error(ex,
                    $"Ошибка при обновлении категории с Id= {categoryDto.Id} и наименованием {categoryDto.Name}");
                return StatusCode(520);
            }
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            var category = await _categoryRepository.GetByIdAsync(id);

            if (category == null)
            {
                return NotFound();
            }

            var products = await _productRepository.ListAsync(p => p.Category == category);
            var articleList = products.Select(p => p.Article).ToList();
            if (articleList.Any())
            {
                return BadRequest($"Категорию невозможно удалить, она используется в товарах: {string.Join("; Артикль№ ", articleList)}");
            }
            try
            {
                await _categoryRepository.DeleteAsync(category);
            return Ok();
            }
            catch (Exception ex)
            {
                Log.Error(ex,
                    $"Ошибка при удалении категории с Id = {id} ");
                return StatusCode(520);
            }
        }

    }

}