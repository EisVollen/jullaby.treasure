﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using JT.DataAccess.Repository;
using JT.Objects.Core;
using JT.Objects.Dto;
using JT.Providers.Builders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace JT.WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProductController : ControllerBase
    {
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<ProductInvoice> _productInvoiceRepository;
        private readonly IMapper _mapper;
        private readonly IObjectCreator<Product> _productCreator;
        private readonly IObjectCreator<Invoice> _invoiceCreator;
        private readonly IObjectCreator<ProductInvoice> _productInvoiceCreator;
        private readonly List<string> _includeCategory = new List<string> {"Category"};

        public ProductController(
            IRepository<Product> productRepository,
            IMapper mapper,
            IObjectCreator<Product> productCreator,
            IObjectCreator<ProductInvoice> productInvoiceCreator,
            IObjectCreator<Invoice> invoiceCreator,
            IRepository<ProductInvoice> productInvoiceRepository)
        {
            _mapper = mapper;
            _productCreator = productCreator;
            _productInvoiceCreator = productInvoiceCreator;
            _invoiceCreator = invoiceCreator;
            _productInvoiceRepository = productInvoiceRepository;
            _productRepository = productRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            //sort by Name
            var products = await _productRepository.ListAsync(orderBy: o => o.OrderBy(p => p.Name),
                includeProperties: _includeCategory);
            //map data
            return new JsonResult(products.Select(product => _mapper.Map<ProductDto>(product)).ToList());
        }


        [HttpGet("article/{article:regex(([[0-9]])+)}")]
        public async Task<IActionResult> GetByArticle(uint article)
        {
            var products =
                await _productRepository.ListAsync(x => x.Article == article, includeProperties: _includeCategory);

            if (products == null)
            {
                return NotFound();
            }

            return new JsonResult(products.Select(product => _mapper.Map<ProductDto>(product)).ToList());
        }

        [HttpGet("category/{id:regex([[0-9A-Fa-f]]{{8}}[[-]]?([[0-9A-Fa-f]]{{4}}[[-]]?){{3}}[[0-9A-Fa-f]]{{12}})}")]
        public async Task<IActionResult> GetByCategory(Guid id)
        {
            var products =
                await _productRepository.ListAsync(x => x.Category.Id == id, 
                    o => o.OrderBy(p => p.Name),
                    _includeCategory);

            if (products == null)
            {
                return NotFound();
            }

            return new JsonResult(products.Select(product => _mapper.Map<ProductDto>(product)).ToList());
        }

        [HttpGet("{text}")]
        public async Task<IActionResult> GetByText(string text)
        {
            var products = await _productRepository.ListAsync(x => x.Name.ToLower().Contains(text.ToLower()) || x.Article.ToString().Contains(text),
                o => o.OrderBy(p => p.Name),
                _includeCategory);

            if (products == null)
            {
                return NotFound();
            }

            return new JsonResult(products.Select(product => _mapper.Map<ProductDto>(product)).ToList());
        }

        [HttpGet("{id:regex([[0-9A-Fa-f]]{{8}}[[-]]?([[0-9A-Fa-f]]{{4}}[[-]]?){{3}}[[0-9A-Fa-f]]{{12}})}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var product = await _productRepository.GetByIdAsync(id, _includeCategory);
            if (product == null)
            {
                return NotFound();
            }

            return new JsonResult(_mapper.Map<ProductDto>(product));
        }
        [HttpGet("invoices/{id:regex([[0-9A-Fa-f]]{{8}}[[-]]?([[0-9A-Fa-f]]{{4}}[[-]]?){{3}}[[0-9A-Fa-f]]{{12}})}")]
        public async Task<IActionResult> GetInvoices(Guid id)
        {
            var product = await _productRepository.GetByIdAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            var productInvoice = await _productInvoiceRepository.ListAsync(p => p.Product == product,
                includeProperties: new List<string> {"Product", "Invoice"});

            return new JsonResult(productInvoice.Select(pi => _mapper.Map<InvoiceWithQuantyDto>(pi)).ToList());
        }

        [HttpPost]
        public async Task<IActionResult> Create([Bind("Name, Article, Cost, Location, Category")]
            ProductDto productDto)
        {
            if (!ModelState.IsValid) return BadRequest();

            var isProductExist =
                await _productRepository.IsExistsAsync(p => p.Article == productDto.Article);
            if (isProductExist) return Conflict("Товар с таким Артиклем уже существует");

            try
            {
                var product = new ProductBuilder(_mapper.Map<Product>(productDto))
                    .AddCategory(_mapper.Map<Category>(productDto.Category))
                    .Build();
                await _productCreator.SaveAsync(product);
                return Ok();
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Ошибка при добавлении {productDto.Name} Арт.№{productDto.Article}");
                return StatusCode(520);
            }
        }

        [HttpPut("ChangeQuantity")]
        public async Task<IActionResult> ChangeQuantity([Bind("ProductId, ChangeQuantity, Number, CreateDate")]
            InvoiceWithQuantyDto invoiceWithQuantyDto)
        {
            if (!ModelState.IsValid) return BadRequest();

            var product =
                await _productRepository.GetAsync(p => p.Id == invoiceWithQuantyDto.ProductId, _includeCategory);
            if (product == null) return NotFound();

            try
            {
              var productInvoice = new ProductInvoiceBuilder(product)
                    .UpdateQuantity(invoiceWithQuantyDto.ChangeQuantity)
                    .Build(_mapper.Map<Invoice>(invoiceWithQuantyDto.Invoice));
                await _productInvoiceCreator.SaveAsync(productInvoice);
                return Ok();
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Ошибка при изменении количества товара Id={invoiceWithQuantyDto.ProductId}");
                return StatusCode(520, ex.Message);
            }
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update([Bind("Id, Name, Article, Cost, Location")] ProductDto productDto)
        {
            if (!ModelState.IsValid) return BadRequest();

            var isProductExist = await _productRepository.IsExistsAsync(p => p.Id == productDto.Id);
            if (!isProductExist)
            {
                return NotFound();
            }

            try
            {
                await _productRepository.Update(_mapper.Map<Product>(productDto));
                return Ok();
            }
            catch (Exception ex)
            {
                Log.Error(ex,
                    $"Ошибка при обновлении товара с Id= {productDto.Id}; {productDto}");
                return StatusCode(520);
            }
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            var product = await _productRepository.GetByIdAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            if (product.Quantity != 0)
            {
                return BadRequest(
                    $"На складе еще имеются остатки товара = {product.Quantity}. Удаление без списания не возможно!");
            }

            try
            {
                var productInvoices = await _productInvoiceRepository.ListAsync(p => p.Product == product,
                    includeProperties: new List<string> { "Product", "Invoice" });
                foreach (var productInvoice in productInvoices)
                {
                    await _productInvoiceRepository.DeleteAsync(productInvoice);
                }
                await _productRepository.DeleteAsync(product);
                return Ok();
                
            }
            catch (Exception ex)
            {
                Log.Error(ex,
                    $"Ошибка при удалении товара с Id = {id} ");
                return StatusCode(520);
            }
        }
    }
}