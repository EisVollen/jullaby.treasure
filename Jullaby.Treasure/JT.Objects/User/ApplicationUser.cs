﻿using Microsoft.AspNetCore.Identity;

namespace JT.Objects.User
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser :IdentityUser
    {
    }
}
