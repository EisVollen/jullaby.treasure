﻿using System;

namespace JT.Objects.Core
{
    public class ProductInvoice: Entity
    {
        public Guid ProductId { get; set; }
        public virtual Product Product { get; set; }
        
        public Guid InvoiceId { get; set; }
        public virtual Invoice Invoice { get; set; }

        public int Quantity { get; set; }
    }
}
