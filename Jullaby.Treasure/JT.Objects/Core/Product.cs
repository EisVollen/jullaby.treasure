﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace JT.Objects.Core
{
    public class Product: Entity
    {
        public string Name { get; set; }
        public uint Article { get; set; }
        public uint Cost { get; set; }
        public uint Quantity { get; set; }
        public string Location { get; set; }
        public DateTime UpdateDate { get; set; }
     
        public Category Category { get; set; }

        public virtual ICollection<ProductInvoice> ProductsInvoices { get; set; }
        [NotMapped]
        public ICollection<Invoice> Invoices => ProductsInvoices.Select(e => e.Invoice).ToArray();
    }
}
