﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace JT.Objects.Core
{
    public class Invoice: Entity
    {
        public string Number { get;set;}
        public DateTime CreateDate { get; set; }

        public ICollection<ProductInvoice> ProductsInvoices { get; set; }
        [NotMapped]
        public IEnumerable<Product> Products => ProductsInvoices.Select(e => e.Product);
    }
}
