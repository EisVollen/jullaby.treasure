﻿using AutoMapper;
using JT.Objects.Core;
using JT.Objects.Dto;

namespace JT.Objects.Mapping
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<Product, ProductDto>();
            CreateMap<ProductDto, Product>();
            
            CreateMap<Invoice, InvoiceDto>();
            CreateMap<InvoiceDto, Invoice>();
                //.ForMember(dest => dest.Products,
                //opt => opt.MapFrom(
                //    src => Mapper.Map<List<Product>,List<ProductDto>>(src))); ;
            CreateMap<Category, CategoryDto>();
            CreateMap<CategoryDto, Category>();

            CreateMap<ProductInvoice, InvoiceWithQuantyDto>();
            CreateMap<InvoiceWithQuantyDto, ProductInvoice>();

        }
    }
}
