﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace JT.Objects.Dto
{
    public class InvoiceWithQuantyDto
    {
        [DisplayName("Id товара")]
        [Required(ErrorMessage = "Укажите Id товара")]
        public Guid ProductId { get; set; }

        [DisplayName("Количество товара в счет-фактуре")]
        [Range(int.MinValue, int.MaxValue, ErrorMessage = "Количество не должно превышать число 2,147,483,647")]
        [RegularExpression("^([0-9-])+$", ErrorMessage = "Количество может состоять только из цифр")]
        [Required(ErrorMessage = "Укажите количество")]
        public int ChangeQuantity { get; set; }

        [DisplayName("Счет-фактура")]
        [Required(ErrorMessage = "Укажите Счет-фактуру")]
        public InvoiceDto Invoice { get; set; }
        
    }
}
