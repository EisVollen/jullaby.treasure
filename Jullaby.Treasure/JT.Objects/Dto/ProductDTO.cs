﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace JT.Objects.Dto
{
    public class ProductDto
    {
        public Guid Id { get; set; }

        [DisplayName("Наименование")]
        [Required(ErrorMessage = "Введите наименование")]
        [MaxLength(200, ErrorMessage = "Длина не должна превышать 200 символов")]
        public string Name { get; set; }

        [DisplayName("Артикль")]
        [Required(ErrorMessage = "Артикль обязателен")]
        [Range(1, uint.MaxValue, ErrorMessage = "Артикль должен быть от 1 до 4,294,967,295")]
        [RegularExpression("^([0-9])+$", ErrorMessage = "Артикль может состоять только из цифр")]
        public uint Article { get; set; }

        [DisplayName("Доступное количество")]
        [Range(0, int.MaxValue, ErrorMessage = "Количество не должно превышать число 2,147,483,647")]
        [RegularExpression("^([0-9])+$", ErrorMessage = "Количество может состоять только из цифр")]
        [Required(ErrorMessage = "Укажите количество")]
        public uint Quantity { get; set; }

        [DisplayName("Цена")]
        [Required(ErrorMessage = "Цена обязателена")]
        [Range(1, uint.MaxValue, ErrorMessage = "Цена не может быть ниже 0 и превышать число 4,294,967,295")]
        [RegularExpression("^([0-9])+$", ErrorMessage = "Цена может содержать только цифры")]
        public uint Cost { get; set; }
        
        [DisplayName("Место нахождения")]
        [Required(ErrorMessage = "Место нахождения обязательно")]
        [MaxLength(1000, ErrorMessage = "Длина не должна превышать 1000 символов")]
        public string Location { get; set; }

        [DisplayName("Категория")]
        [Required(ErrorMessage = "Укажите Категорию")]
        public CategoryDto Category { get; set; }

        [DisplayName("Дата обновления")]
        public DateTime UpdateDate { get; set; }

    }
}
