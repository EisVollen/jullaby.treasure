﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace JT.Objects.Dto
{
    /// <summary>
    /// Счет-фактура
    /// Приходная и расходная накладная
    /// </summary>
    public class InvoiceDto
    {
        [DisplayName("Номер счет-фактуры")]
        [RegularExpression(@"([-—,/\\:.A-Za-zА-Яа-яЁё0-9])*", ErrorMessage =
            "Номер счет-фактуры может содержать буквы русского и английского алфавита, знаки препинания, цифры.")]
        [Required(ErrorMessage = "Укажите номер счет-фактуры")]
        [MaxLength(100, ErrorMessage = "Длина не должна превышать 100 символов")]
        public string Number { get; set; }

        [DisplayName("Дата счет-фактуры")]
        [Required(ErrorMessage = "Укажите дату счет-фактуры")]
        public DateTime CreateDate { get; set; }

    }
}
