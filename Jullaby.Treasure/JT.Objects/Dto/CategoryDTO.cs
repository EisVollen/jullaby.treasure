﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace JT.Objects.Dto
{
    public class CategoryDto
    {
        public Guid Id { get; set; }

        [DisplayName("Наименование")]
        [Required(ErrorMessage = "Введите наименование")]
        [MaxLength(200, ErrorMessage = "Длина не должна превышать 200 символов")]
        public string Name { get; set; }
    }
}
