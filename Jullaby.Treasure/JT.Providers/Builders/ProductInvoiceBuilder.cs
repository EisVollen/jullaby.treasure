﻿using System;
using JT.Objects.Core;

namespace JT.Providers.Builders
{
    public class ProductInvoiceBuilder
    {
        private readonly Product _product;
        private readonly ProductInvoice _productInvoice = new ProductInvoice();

        public ProductInvoiceBuilder(Product product)
        {
            _product = product;
        }
        public ProductInvoiceBuilder UpdateQuantity(int quantity)
        {
            var total = _product.Quantity + quantity;
            if (total < 0) throw new Exception("Общее количество не может быть меньше 0");
            if (total > uint.MaxValue) throw new Exception("Общее количество не может быть больше 4,294,967,295");
            _product.Quantity = (uint) total;
            _product.UpdateDate = DateTime.Now;
            _productInvoice.Quantity = quantity;
            return this;
        }

        public ProductInvoice Build(Invoice invoice)
        {
            _productInvoice.Product = _product;
            _productInvoice.Invoice = invoice;
            return _productInvoice;
        }
    }
}
