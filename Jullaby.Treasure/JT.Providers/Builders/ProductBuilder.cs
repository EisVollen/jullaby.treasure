﻿using JT.Objects.Core;

namespace JT.Providers.Builders
{
    public class ProductBuilder
    {
        private readonly Product _product;

        public ProductBuilder(Product product)
        {
            _product = product;
        }
        public ProductBuilder AddCategory(Category category)
        {
          _product.Category = category;
            return this;
        }

        public Product Build()
        {
            return _product;
        }
    }
}
